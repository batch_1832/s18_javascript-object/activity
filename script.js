// console.log("Hello")




let trainer = {
	name: "Ash Ketchum",
	age: 10,
	pokemon:["Pikachu", "Charizard", "Squirtle", "Bulbasaur"],
	friends: {
		hoenn: ["May", "Max"],
		kanto: ["Brock", "Misty"]

		},
		talk: function(){
			console.log(this.pokemon[0]+ "!"+ "I choose you!")
		}
	
}
console.log(trainer);
console.log(trainer.friends);
console.log(trainer.pokemon);
console.log("Result of dot notation: ");
console.log(trainer.name);
console.log("Result of bracket notation: ");
console.log(trainer["pokemon"]);
console.log("Result of talk method: ");
trainer.talk();


let myPokemon = {
	name: "Pikachu",
	level: 3,
	health: 100,
	attack: 50,
	tackle: function(){
		console.log("This pokemon tackeled targetPokemon");
		console.log("targetPokemon's health is reduced to newTargetPokemonHealth");
	},
	faint: function(){
		console.log("Pokemon fainted");
	}

}
console.log(myPokemon);

//Object Constractor Notation
function Pokemon(name, level){

	this.name = name;
	this.level = level;
	this.health = level * 2;
	this.attack = level;

	this.tackle = function(target){     
		console.log(this.name + " " + " tackled "
	    + target.name);
		console.log("targetPokemon's health is reduced to newTargetPokemonHealth");

		target.faint();
	}

	this.faint = function(){
		console.log(this.name+ " fainted. ");

	}

}

let pikachu = new Pokemon("Pikachu", 12);
console.log(pikachu)

let geodude = new Pokemon("Geodude", 8);
console.log(geodude);

let mewtwo = new Pokemon("Mewtwo", 100);
console.log(mewtwo);


geodude.tackle(pikachu);
mewtwo.tackle(geodude);
geodude.tackle(mewtwo);